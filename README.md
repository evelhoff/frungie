
Eveline Hoffelinck
Yentl Longueville
Opleidingsonderdeel: Web Design IV
Academiejaar: 2016-17
Opleiding: Bachelor in de grafische en digitale media
Afstudeerrichting: Crossmedia-Ontwerp
Opleidingsinstelling: Arteveldehogeschool


## Installatie

Voer volgende commando uit voor je de jekyll kan starten

```
bundle install
```

## Starten van de Jekyll website in powershell met de DotFiles

```
bundle exec jekyll serve
```



